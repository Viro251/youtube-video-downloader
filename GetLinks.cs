using System;
using System.Web;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Xml;
using YoutubeExtractor;
using System.Diagnostics; // For using stopwatch
using System.Linq; // For casting XmlNodeList to List
using System.Threading.Tasks;

namespace YoutubeDownloader
{
    public class GetLinks
    {
        public string MainDirectoryPath { get => mainDirectoryPath; set => mainDirectoryPath = value; }
        public string ConfigPath { get => configPath; set => configPath = value; }
        public string PathToLinksInJson { get => pathToLinksInJson; set => pathToLinksInJson = value; }
        public string PathToLinksInTxt { get => pathToLinksInTxt; set => pathToLinksInTxt = value; }
        public string PathToLinksInXml { get => pathToLinksInXml; set => pathToLinksInXml = value; }

        private string mainDirectoryPath = AppDomain.CurrentDomain.BaseDirectory;
        private string configPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "userfiles/config.xml");
        private string pathToLinksInXml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "userfiles/links.xml");
        private string pathToLinksInJson = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "userfiles/links.json");
        private string pathToLinksInTxt = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "userfiles/links.txt");

        private static string downloadFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "download");
        private static string inputType;

        public static string GetDownloadFolder()
        {
            return downloadFolder;
        }
        public static void SetDownloadFolder(string value)
        {
            downloadFolder = value;
        }
        public static string GetInputType()
        {
            return inputType;
        }
        public static void SetInputType(string value)
        {
            inputType = value;
        }

        public void showMainDirectoryPath()
        {
            System.Console.WriteLine("Main directory: " + MainDirectoryPath);
            System.Console.WriteLine("");
        }
        public void readConfig()
        {
            try
            {
                XmlDocument cfgxml = new XmlDocument();
                cfgxml.Load(ConfigPath);
                XmlNode root = cfgxml.FirstChild;

                //Read, assign and show downloadFolder
                SetDownloadFolder(downloadFolder + (root["downloadFolder"].InnerText));
                System.Console.WriteLine("Selected download folder is: " + GetDownloadFolder());
                System.Console.WriteLine("");

                //Read, assign and show inputType
                SetInputType(root["inputType"].InnerText);
                if (inputType == "json" || inputType == "txt" || inputType == "xml")
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    System.Console.WriteLine("Selected input type is: " + GetInputType());
                    System.Console.WriteLine("");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    System.Console.WriteLine("Warning! Entered wrong input type");
                    Console.ResetColor();
                    Console.ReadKey();
                }
            }

            catch (System.IO.DirectoryNotFoundException ex01)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Warning!. The config xml file must be placed in 'userfiles' folder and must be named 'config' (in format xml).");
                Console.ResetColor();
                System.Console.WriteLine("");
                System.Console.WriteLine("Exception " + ex01);
            }
            catch (System.NullReferenceException ex02)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Warning!. The specified node can not be found");
                Console.ResetColor();
                System.Console.WriteLine("");
                System.Console.WriteLine("Exception " + ex02);
            }

        }
        public void GetLink()
        {
            if (inputType == "json")
            {
                GetLinksFromJson();
            }
            else if (inputType == "txt")
            {
                GetLinkFromTxtFile();
            }
            else if (inputType == "xml")
            {
                GetLinksFromXmlFile();
            }
        }
        public void GetLinksFromJson()
        {
            List<Link> ytLinks = JsonConvert.DeserializeObject<List<Link>>(File.ReadAllText(PathToLinksInJson));

            foreach (var links in ytLinks)
            {
                System.Console.WriteLine(links.link);
                System.Console.WriteLine("");
            }
        }
        public void GetLinkFromTxtFile()
        {
            string[] links = File.ReadAllLines(pathToLinksInTxt);
            foreach (string line in links)
            {
                Console.WriteLine(line);
                System.Console.WriteLine("");
            }
        }
        public void GetLinksFromXmlFile()
        {
            XmlDocument linksXml = new XmlDocument();
            linksXml.Load(pathToLinksInXml);
            XmlNodeList linksList = linksXml.SelectNodes("/root/link");
            foreach (XmlNode link in linksList)
            {
                System.Console.WriteLine(link.InnerText);
                System.Console.WriteLine("");
            }

        }
        public void Download()
        {
            if (inputType == "json")
            {
                DownloadFilesFromJson();
            }
            else if (inputType == "txt")
            {
                DownloadFilesFromTxt();
            }
            else if (inputType == "xml")
            {
                DownloadFilesFromXml();
            }

        }
        public void DownloadFilesFromJson()
        {
            try
            {
                //Reading json part
                List<Link> ytLinks = JsonConvert.DeserializeObject<List<Link>>(File.ReadAllText(PathToLinksInJson));
                Stopwatch swWholeLoop = Stopwatch.StartNew();
                Parallel.ForEach(ytLinks, new ParallelOptions { MaxDegreeOfParallelism = 5 }, links =>
               {
                   //Time flow meter
                   Stopwatch sw = Stopwatch.StartNew();

                   IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(links.link, false);
                   YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                   _youtubeDownloader.DownloadVideo(videoInfos);

                   //Time flow meter
                   System.Console.WriteLine("");
                   Console.ForegroundColor = ConsoleColor.DarkYellow;
                   System.Console.WriteLine("Downloading finished in {0} sec.", sw.ElapsedMilliseconds / 1000.0);
                   System.Console.WriteLine("");
                   Console.ResetColor();
                   System.Console.WriteLine("");
               }
               );
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                System.Console.WriteLine("Downloading all files took {0} sec.", swWholeLoop.ElapsedMilliseconds / 1000.0);
                Console.ResetColor();

            }
            catch (YoutubeExtractor.YoutubeParseException ex05)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Error with parser. Try again", Environment.NewLine);
                Console.ResetColor();
                System.Console.WriteLine(ex05);
            }

        }
        public void DownloadFilesFromTxt()
        {
            try
            {
                //Reading txt part
                string[] links = File.ReadAllLines(pathToLinksInTxt);

                Stopwatch swWholeLoop = Stopwatch.StartNew();

                Parallel.ForEach(links, new ParallelOptions { MaxDegreeOfParallelism = 5 }, line =>
                {
                    //Time flow meter
                    Stopwatch sw = Stopwatch.StartNew();

                    IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(line, false);
                    YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                    _youtubeDownloader.DownloadVideo(videoInfos);

                    //Time flow meter
                    System.Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    System.Console.WriteLine("Downloading finished in {0} sec.", sw.ElapsedMilliseconds / 1000.0);
                    System.Console.WriteLine("");
                    Console.ResetColor();
                    System.Console.WriteLine("");
                });
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                System.Console.WriteLine("Downloading all files took {0} sec.", swWholeLoop.ElapsedMilliseconds / 1000.0);
                Console.ResetColor();
            }
            catch (YoutubeExtractor.YoutubeParseException ex05)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Error with parser. Try again", Environment.NewLine);
                Console.ResetColor();
                System.Console.WriteLine(ex05);
            }
        }
        public void DownloadFilesFromXml()
        {
            try
            {
                //Reading xml part
                XmlDocument linksXml = new XmlDocument();
                linksXml.Load(pathToLinksInXml);
                XmlNodeList linksList = linksXml.SelectNodes("/root/link");

                Stopwatch swWholeLoop = Stopwatch.StartNew();

                Parallel.ForEach(linksList.Cast<XmlNode>(), new ParallelOptions { MaxDegreeOfParallelism = 5 }, (XmlNode link) =>
                {
                    //Time flow meter
                    Stopwatch sw = Stopwatch.StartNew();

                    IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link.InnerText, false);
                    YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                    _youtubeDownloader.DownloadVideo(videoInfos);

                    //Time flow meter
                    System.Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    System.Console.WriteLine("Downloading finished in {0} sec.", sw.ElapsedMilliseconds / 1000.0);
                    System.Console.WriteLine("");
                    Console.ResetColor();
                    System.Console.WriteLine("");
                });

                Console.ForegroundColor = ConsoleColor.DarkGreen;
                System.Console.WriteLine("Downloading all files took {0} sec.", swWholeLoop.ElapsedMilliseconds / 1000.0);
                Console.ResetColor();
                // Working code
                // Reading xml part

                // foreach (XmlNode link in linksList)
                // {
                //     IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link.InnerText, false);
                //     YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                //     _youtubeDownloader.DownloadVideo(videoInfos);
                // }
            }
            catch (YoutubeExtractor.YoutubeParseException ex05)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Error with parser. Try again", Environment.NewLine);
                Console.ResetColor();
                System.Console.WriteLine(ex05);
            }
        }



    }
}
