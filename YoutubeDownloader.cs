using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using YoutubeExtractor;



namespace YoutubeDownloader
{
    public class YoutubeDownloader : GetLinks
    {

        public void DownloadVideo(IEnumerable<VideoInfo> videoInfos)
        {
            /*
             * Select the first .mp4 video with 360p resolution
             */
            VideoInfo video = videoInfos
                .First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360);

            /*
             * If the video has a decrypted signature, decipher it
             */
            if (video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }

            /*
             * Create the video downloader.
             * The first argument is the video to download.
             * The second argument is the path to save the video file.
             */
            try
            {

                //This will create directory if it not exist and it will not bug if directory already exist.
                System.IO.Directory.CreateDirectory(GetDownloadFolder());

                System.Console.WriteLine("The download folder right before starting downloading: " + GetDownloadFolder());
                var videoDownloader = new VideoDownloader(video,


                 Path.Combine(GetDownloadFolder(), RemoveIllegalPathCharacters(video.Title) + video.VideoExtension));

                // // Register the ProgressChanged event and print the current progress
                // videoDownloader.DownloadProgressChanged += (sender, args) => Console.WriteLine(args.ProgressPercentage);

                /*
                 * Execute the video downloader.
                 * For GUI applications note, that this method runs synchronously.
                 */
                videoDownloader.Execute();

            }

            catch (System.IO.DirectoryNotFoundException ex03)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Warning! There is no such a directory.");
                Console.ResetColor();
                System.Console.WriteLine("");
                System.Console.WriteLine(ex03);
                System.Console.WriteLine("");
            }

        }




        public string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }
    }
}